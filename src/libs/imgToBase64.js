export const imgToBase64 = (file) => {

    return new Promise((resolve, reject) => {

        var imageType = /image.*/;

        if (file.type.match(imageType)){

            var filesize = file.size;

            if(filesize <= 1048576){
                var reader = new FileReader();
                reader.readAsDataURL(file);

                reader.onerror = () => { reject(new Error("Error")) };
                reader.onload = () => { resolve(reader.result) };
            }else{
                reject(new Error("Max. image size is 1MB."))
            }

        }else{
            reject(new Error("Please select an image."));
        }

    })

}

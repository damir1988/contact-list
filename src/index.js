import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from './store';
import history from './libs/history';

import Home from './screens/Home';
import Details from './screens/Details';
import Edit from './screens/Edit';
import New from './screens/New';

import './assets/sass/styles.sass';


class App extends React.Component {

    render(){
        return(
            <Router history={history}>
                <Route exact path="/" component={Home} />
                <Route path="/details/:id" component={Details} />
                <Route path="/edit/:id" component={Edit} />
                <Route path="/new" component={New} />
            </Router>
        )
    }

}

ReactDOM.render(<Provider store={store}><App/></Provider>, document.getElementById('root'));

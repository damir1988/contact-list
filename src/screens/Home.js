import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { ActionCreators } from "../actions";
import { sortContacts } from "../reducers";

import Header from "../components/Header";
import ContactsList from "../components/ContactsList";
import Modal from "../components/Modal";


class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            search: '',
            favorites: false,
        }
    }


    componentDidMount() {
        this.props.getContacts();
    }

    searchChanged(event) {

        this.setState({
            search: event.target.value
        })

    }
    render(){

        return(
            <div>
                <Header/>
                <article>
                    <div className="filter">
                        <button className={"filter__item " + (!this.state.favorites ? "filter__item--active" : "")} onClick={() => { this.setState({ favorites: false }) }}>All contacts</button>
                        <button className={"filter__item " + (this.state.favorites ? "filter__item--active" : "")} onClick={() => { this.setState({ favorites: true }) }}>My favorites</button>
                    </div>
                    <div className="search">
                        <input type="text" className="search__input" value={this.state.search}  onChange={(event) => { this.searchChanged(event); }} autoFocus />
                    </div>
                    <div className="container">
                        <ContactsList data={this.props.contacts} search={this.state.search} favorites={this.state.favorites}/>
                    </div>
                </article>
                 <Modal/>
            </div>
        )

    }

}

function mapStateToProps(state){

    return {
        contacts: sortContacts(state.contacts)
    };
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);

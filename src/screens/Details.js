import React from 'react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { ActionCreators } from "../actions";

import Header from "../components/Header";
import IconEdit from "../assets/images/icon-edit.svg";
import IconEmail from "../assets/images/icon-email.svg";
import IconPhone from "../assets/images/icon-phone.svg";
import ButtonFavorite from "../components/ButtonFavorite";
import ButtonBack from "../components/ButtonBack";


class Details extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            favorites: false,
        }
    }

    componentDidMount() {

        this.props.getContact(parseInt(this.props.match.params.id));

    }

    render(){

        let { id, name, favorite, email, numbers, image } = this.props.contact ? this.props.contact : '';
        let numbersElement = null;

        if(numbers){
            numbersElement = Object.values(numbers).map((item, index) => {
                return (
                    <div className="contact__info" key={index}>
                        <div className="contact__infolabel">{item.label}</div>
                        <a href={"tel:" + item.number} className="contact__infoval">{item.number}</a>
                    </div>
                )
            })
        }

        return(
            <div>
                <Header/>
                <div className="topnav">
                    <ButtonBack buttonClass={"topnav__button"} imgClass={"topnav__img"}/>
                    <ButtonFavorite contactid={id} favorite={favorite} buttonClass={"topnav__button"} imgClass={"topnav__img"} single={true}/>
                    <Link to={'/edit/' + id} className="topnav__button"><img src={IconEdit} alt="" className="topnav__img"/></Link>
                </div>
                <article>
                    <div className="container">
                        <div className="contact">
                            <div className="contact__heading">
                                <div className="contact__imgwrap contactlist__imgwrap" style={{ backgroundImage: `url(${image})`}}></div>
                                <ButtonBack buttonClass={"topnav__button visible-sm"} imgClass={"topnav__img topnav__img--back"}/>
                                <h1 className="contactlist__title contact__title">{name}</h1>
                                <ButtonFavorite contactid={id} favorite={favorite} buttonClass={"topnav__button visible-sm"} imgClass={"topnav__img"} single={true}/>
                                <Link to={'/edit/' + id} className="topnav__button visible-sm"><img src={IconEdit} alt="" className="topnav__img"/></Link>
                            </div>
                            <div className="contact__item">
                                <div className="contact__label contact__label--email">
                                    <img src={IconEmail} alt="" className="contact__labelimg"/>
                                    email
                                </div>
                                <div className="contact__infowrap">
                                    <div className="contact__info">
                                        <a href={"mailto:" + email} className="contact__infoval contact__infoval--email">{email}</a>
                                    </div>
                                </div>
                            </div>
                            <div className="contact__item">
                                <div className="contact__label">
                                    <img src={IconPhone} alt="" className="contact__labelimg"/>
                                    numbers
                                </div>
                                <div className="contact__infowrap">
                                    {numbersElement}
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        )

    }

}

function mapStateToProps(state){
    return {
        contact: state.contact,
    };
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Details);

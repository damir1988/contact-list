import React from 'react';
import { withRouter } from "react-router-dom";
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { ActionCreators } from "../actions";

import Header from "../components/Header";
import Modal from "../components/Modal";
import ButtonBack from "../components/ButtonBack";
import Form from "../components/Form";
import ButtonDelete from "../components/ButtonDelete";


class Edit extends React.Component {

    render(){

        let id = this.props.match.params.id;

        return(
            <div>
                <Header/>
                <div className="topnav">
                    <ButtonBack buttonClass={"topnav__button"} imgClass={"topnav__img topnav__img--back"}/>
                    <ButtonDelete contactid={id} buttonClass={"topnav__button"} imgClass={"topnav__img"}/>
                </div>
                <article>
                    <div className="container">
                        <Form edit={true}/>
                    </div>
                </article>
                <Modal goBack={true}/>
            </div>
        )

    }

}

export default Edit;

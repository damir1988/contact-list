import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { ActionCreators } from "../actions";

import Header from "../components/Header";
import Form from "../components/Form";
import IconBack from "../assets/images/icon-back.svg";


class New extends React.Component {

    render(){

        return(
            <div>
                <Header/>
                <div className="topnav">
                    <Link to={'/'} className="topnav__button"><img src={IconBack} alt="" className="topnav__img topnav__img--back"/></Link>
                </div>
                <article>
                    <div className="container">
                        <Form/>
                    </div>
                </article>
            </div>
        )

    }

}

function mapStateToProps(state){
    return {
        contacts: state.contacts
    };
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(New));

export const GET_CONTACTS = 'GET_CONTACTS';
export const GET_CONTACT = 'GET_CONTACT';
export const SET_DELETE_ID = 'SET_DELETE_ID';
export const SET_SAVED = 'SET_SAVED';

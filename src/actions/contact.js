import * as types from './types';
import history from '../libs/history';
import localStorageQuota from '../libs/localStorageQuota';


/* Get contacts */
export function getContacts() {
    return {
        type: types.GET_CONTACTS,
    }
}

/* Get contact */
export function getContact(contactId){
    return {
        type: types.GET_CONTACT,
        payload: contactId,
    }
}

/* Set saving */
export function setSaved(saved){
    return {
        type: types.SET_SAVED,
        payload: saved,
    }
}

/* Add contact */
export function addContact({ contact }){
    return (dispatch) => {

        var contacts = JSON.parse(localStorage.getItem("@contactlist"));

        contacts = contacts ? contacts : [];
        contact.id = new Date().getTime();
        contacts.push(contact);


        try{
            localStorage.setItem("@contactlist", JSON.stringify(contacts));
            history.go(-1);
        }catch(e){
            if(localStorageQuota(e)){
                console.log('Storage full');
            }
        }

    }
}

/* Edit contact */
export function editContact({ contact }){

    return (dispatch) => {

        var contacts = JSON.parse(localStorage.getItem("@contactlist"));

        var editedContacts = contacts.map(item => {
            if(item.id === contact.id){
                item = contact;
            }
            return item;
        });


        try{
            localStorage.setItem("@contactlist", JSON.stringify(editedContacts));
            dispatch(getContact(contact.id));
            dispatch(setSaved(true));
        }catch(e){
            if(localStorageQuota(e)){
                console.log('Storage full');
            }
        }

    }

}

/* Delete contact */
export function deleteContact(goBack){

    return(dispatch, getState) => {

        var contacts = JSON.parse(localStorage.getItem("@contactlist"));
        var filteredContacts = contacts.filter((contact) => {
            return contact.id !== parseInt(getState().deleteId);
        });

        try{

            localStorage.setItem("@contactlist", JSON.stringify(filteredContacts));
            dispatch(setDeleteId(null));

            if(goBack) {
                history.go(-2);
            }else{
                dispatch(getContacts());
            }

        }catch(e){
            if(localStorageQuota(e)){
                console.log('Storage full');
            }
        }

    }

}

/* Add to favorites */
export function toggleFavorite(contactId, single){

    return(dispatch) => {
        var contacts = JSON.parse(localStorage.getItem("@contactlist"));

        var editedContacts = contacts.map(item => {
            if(item.id === contactId){
                item.favorite = !item.favorite;
            }
            return item;
        });

        try{

            localStorage.setItem("@contactlist", JSON.stringify(editedContacts));
            single ? dispatch(getContact(contactId)) : dispatch(getContacts());

        }catch(e){
            if(localStorageQuota(e)){
                console.log('Storage full');
            }
        }

    }
}

/* Set delete id */
export function setDeleteId(deleteId){
    return{
        type: types.SET_DELETE_ID,
        payload: deleteId
    }
}

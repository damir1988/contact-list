import React from "react";
import { withRouter } from "react-router-dom";
import {bindActionCreators} from "redux";
import {ActionCreators} from "../actions";
import {connect} from "react-redux";

import IconBack from "../assets/images/icon-back.svg";

class ButtonBack extends React.Component{

    render() {

        return(
            <button className={this.props.buttonClass} type="button"><img src={IconBack} alt="" className={this.props.imgClass} onClick={() => { this.props.history.goBack() }}/></button>
        )
    }

}

function mapStateToProps(state){
    return state;
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ButtonBack));

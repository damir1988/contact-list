import React from "react";
import {Link} from 'react-router-dom';

import ButtonFavorite from '../components/ButtonFavorite';
import ButtonDelete from '../components/ButtonDelete';
import iconEdit from "../assets/images/icon-edit.svg";

const ContactItem = (props) => {

    return(
        <li>
            <div className="contactlist__item">
                <Link className="contactlist__col" to={'/details/' + props.data.id}>
                    <div className="contactlist__imgwrap" style={{ backgroundImage: `url(${props.data.image})`}}></div>
                    <h2 className="contactlist__title">{props.data.name}</h2>
                </Link>
                <div className="contactlist__col">
                    <ButtonFavorite contactid={props.data.id} favorite={props.data.favorite} buttonClass={"contactlist__button contactlist__button--add"} imgClass={""}/>
                    <Link to={'/edit/' + props.data.id} className="contactlist__button contactlist__button--edit"><img src={iconEdit} alt=""/></Link>
                    <ButtonDelete contactid={props.data.id} buttonClass={"contactlist__button contactlist__button--delete"} />
                </div>
            </div>
        </li>
    )

}

export default ContactItem;

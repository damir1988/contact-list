import React from "react";
import { Link } from "react-router-dom";

import ContactItem from "./ContactsItem";

const ContactList = (props) => {

    var filteredData;
    if(props.data){
        filteredData = Object.values(props.data).filter(contact => {

            var contactSearch = contact.name.toLowerCase();

            if(!props.favorites){
                return contactSearch.indexOf(props.search) !== -1;
            }else{
                return (contactSearch.indexOf(props.search) !== -1 && contact.favorite === true)
            }
        })
    }

    return(
        <ul className="contactlist">
            {
                !props.favorites ? (
                    <li>
                        <Link to={'/new'} className="btnadd f--regular">Add new</Link>
                    </li>
                ) : (null)
            }
            {
                props.data ? (
                    filteredData.map(contact => {
                        return <ContactItem key={contact.id} data={contact}/>
                    })
                ) : (null)
            }
        </ul>
    )

}

export default ContactList;

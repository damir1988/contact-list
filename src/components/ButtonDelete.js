import React from "react";
import { withRouter } from "react-router-dom";
import {bindActionCreators} from "redux";
import {ActionCreators} from "../actions";
import {connect} from "react-redux";

import IconDelete from "../assets/images/icon-delete.svg";

class ButtonDelete extends React.Component{

    deletePressed(){

        this.props.setDeleteId(this.props.contactid);

    }

    render() {

        return(
            <button className={this.props.buttonClass} type="button" onClick={() => { this.deletePressed(); }}>{this.props.children}<img src={IconDelete} alt="" className={this.props.imgClass}/></button>
        )
    }

}

function mapStateToProps(state){
    return state;
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ButtonDelete));

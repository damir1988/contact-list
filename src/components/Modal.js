import React from "react";
import {bindActionCreators} from "redux";
import {ActionCreators} from "../actions";
import {connect} from "react-redux";

class Modal extends React.Component {

    render(){

        return(
            <div className={"modal " + (this.props.deleteId !== null ? "modal--opened" : "")}>
                <div className="modal__dialog">
                    <div className="modal__heading">
                        <h3 className="modal__title">Delete</h3>
                    </div>
                    <div className="modal__content">
                        <p>Are you sure you want to delete this contact?</p>
                    </div>
                    <div className="modal__footer">
                        <button className="button button--default" type="button" onClick={()=>{ this.props.setDeleteId(null);}}>Cancel</button>
                        <button className="button button--primary" type="button" onClick={() => { this.props.deleteContact(this.props.goBack); }}>Delete</button>
                    </div>
                </div>
            </div>
        )

    }

}

function mapStateToProps(state){
    return {
        deleteId: state.deleteId
    };
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Modal);

import React from "react";
import { withRouter } from "react-router-dom";
import {bindActionCreators} from "redux";
import {ActionCreators} from "../actions";
import {connect} from "react-redux";
import {imgToBase64} from "../libs/imgToBase64";

import ButtonBack from "./ButtonBack";
import ButtonDelete from "./ButtonDelete";
import IconPerson from "../assets/images/icon-person.svg";
import FormControl from "./FormControl";
import IconEmail from "../assets/images/icon-email.svg";
import IconPhone from "../assets/images/icon-phone.svg";
import IconRemove from "../assets/images/icon-remove.svg";
import IconAdd from "../assets/images/icon-add.svg";


class Form extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            form: {
                favorite: false,
                numbers: {},
                image: ''
            },
            hovered: false,
        }
    }

    componentDidMount() {

        if(this.props.edit)this.props.getContact(parseInt(this.props.match.params.id));

    }

    handleChange(event, index) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        if(typeof index !== "undefined"){

            this.setState({

                form: { ...this.state.form, numbers: { ...this.state.form.numbers, [index]: {...this.state.form.numbers[index], [name]: value} } }

            })
        }else{
            this.setState({
                form: { ...this.state.form, [name]: value }
            });
        }

    }

    handleSubmit(event) {
        event.preventDefault();
        if(this.props.edit){
            this.props.editContact({ contact: this.state.form })
        }else{
            this.props.addContact({ contact: this.state.form })
        }
    }

    addNumber(){

        var index = Object.keys(this.state.form.numbers).length;

        this.setState({
            form: {
                ...this.state.form,
                numbers: { ...this.state.form.numbers, [index]: {} }
            }
        });

    }

    removeNumber(index){

        var newNumbers = Object.values(this.state.form.numbers).filter((item, key) => { return key !== index; });

        this.setState({
            form: {
                ...this.state.form,
                numbers: newNumbers
            }
        });

    }

    handleHover(hovered, id){

        hovered ?  this.refs["numberwrap" + id].className="form__numberwrap form__numberwrap--hovered" : this.refs["numberwrap" + id].className="form__numberwrap";

    }

    handleImgUpload(event){

        var file = event.target.files[0];
        if(file){
            imgToBase64(file).then((result) => {

                this.setState({
                    form:{
                        ...this.state.form,
                        image: result
                    }
                })

            }).catch((ex) => {
                console.log(ex);
            })
        }

    }

    removeImg(event){
        event.preventDefault();
        this.setState({
            form: {
                ...this.state.form,
                image: ''
            }
        })

    }

    componentWillReceiveProps(nextProps, nextContext) {

        let contact = nextProps.contact;
        this.setState({
            form:{
                ...this.state.form,
                id: contact.id,
                name: contact.name,
                email: contact.email,
                numbers: contact.numbers,
                favorite: contact.favorite,
                image: contact.image,
            }
        })

        if(nextProps.saved == true && this.props.saved !== nextProps.saved){
            this.refs["savebutton"].className = "button button--primary button__save button__save--saved";

            clearTimeout(this.timer)
            this.timer = setTimeout(()=> {
                this.refs["savebutton"].className = "button button--primary button__save";
                this.props.setSaved(false);
            }, 1000)

        }

    }

    componentWillUnmount() {

        this.props.setSaved(false);
        clearTimeout(this.timer);

    }

    render() {

        let { id, name, email, numbers, image } = this.state.form;

        return(
            <form action="" className="form" onSubmit={(event) => { this.handleSubmit(event); }}>
                <div className="contact">
                    <div className="contact__heading form__heading">
                        <div className="contactlist__imgwrap contact__imgwrap form__imgwrap" style={{ backgroundImage: `url(${image})`}}>
                            {
                                !image ? (
                                    <label htmlFor="imagefile" className="form__imglabel">
                                        <input type="file" id="imagefile" className="form__controlimg" onChange={(event) => { this.handleImgUpload(event); }}/>
                                        <input type="hidden" name="image" id="image" value={image}/>
                                    </label>
                                ) : (
                                    <a href={"#"} type="button" className="form__imgremove" onClick={(event) => { this.removeImg(event) }} ref={"imgremove"}></a>
                                )
                            }
                        </div>
                        <ButtonBack buttonClass={"topnav__button visible-sm"} imgClass={"topnav__img topnav__img--back"}/>
                        <div className="contactlist__title contact__title visible-sm invisible">{}</div>
                        {
                            this.props.edit ? (<ButtonDelete contactid={id} buttonClass={"topnav__button visible-sm visible-sm--flex f--regular pull-right--flex"} imgClass={"topnav__img topnav__img--text"}>Delete</ButtonDelete>) : (null)
                        }
                    </div>
                    <div className="contact__item form__group">
                        <label htmlFor="name" className="contact__label form__label">
                            <img src={IconPerson} alt="" className="contact__labelimg"/>
                            full name
                        </label>
                        <div className="form__col">
                            <FormControl type="text" name="name" id="name" required={true} className="form__control" value={name || ''} valueChanged={(event) => { this.handleChange(event); }}/>
                        </div>
                    </div>
                    <hr/>
                    <div className="contact__item form__group">
                        <label htmlFor="email" className="contact__label form__label">
                            <img src={IconEmail} alt="" className="contact__labelimg"/>
                            email
                        </label>
                        <div className="form__col">
                            <FormControl type="email" name="email" id="email" className="form__control" value={email || ''} valueChanged={(event) => { this.handleChange(event); }}/>
                        </div>
                    </div>
                    <hr/>
                    <div className="contact__item form__group">
                        <label htmlFor="number-1" className="contact__label form__label">
                            <img src={IconPhone} alt="" className="contact__labelimg"/>
                            numbers
                        </label>
                        {

                            Object.values(numbers).map((number, index) => {
                                return(
                                    <div className="form__numberwrap" key={index} ref={"numberwrap" + index}>
                                        <div className="form__col">
                                            <FormControl type="text" name="number" id={"number-" + index} className="form__control" value={number[Object.keys(number)[0]] || ''} valueChanged={(event) => { this.handleChange(event, index); }} required={true} placeholder={"number"}/>
                                        </div>
                                        <div className="form__col">
                                            <FormControl type="text" name="label" id={"label-" + index} className="form__control" value={number[Object.keys(number)[1]] || ''} valueChanged={(event) => { this.handleChange(event, index); }} required={true} placeholder={"label"}/>
                                            <button type="button" className="form__removebtn" onMouseEnter={() => { this.handleHover(true, index) }} onMouseLeave={() => { this.handleHover(false, index) }} onClick={() => { this.removeNumber(index); }}><img src={IconRemove} alt="" className="form__removeimg"/></button>
                                        </div>
                                        <hr className="invisible hralt"/>
                                    </div>
                                )

                            })

                        }
                    </div>
                    <div className="contact__item form__group">
                        <div className="form__col">
                            <button className="buttonadd" type="button" onClick={() => { this.addNumber() }}>
                                <div className="buttonadd__imgwrap">
                                    <img src={IconAdd} alt="" className="buttonadd__img"/>
                                </div>
                                Add number
                            </button>
                        </div>
                    </div>
                    <div className="contact__item form__group">
                        <div className="form__col form__col--block">
                            <div className="button__group">
                                <button className="button button--default" type="button" onClick={() => { this.props.history.goBack() }}>Cancel</button>
                                <button className="button button--primary button__save" type="submit" ref={"savebutton"}>
                                    <span>Save</span>
                                    <span>Saved!</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }

}

function mapStateToProps(state){
    return {
        contact: state.contact,
        saved: state.saved,
    };
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Form));

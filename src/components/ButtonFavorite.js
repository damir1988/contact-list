import React from "react";
import {bindActionCreators} from "redux";
import {ActionCreators} from "../actions";
import {connect} from "react-redux";

import iconFavoriteEmpty from "../assets/images/icon-favorite-empty.svg";
import iconFavorite from "../assets/images/icon-favorite.svg";

class ButtonFavorite extends React.Component{

    toggleFavoritePressed(){
        this.props.toggleFavorite(this.props.contactid, this.props.single);
    }

    render() {


        return(
            <button type="button" className={this.props.buttonClass} onClick={() => { this.toggleFavoritePressed(); }}>
                {
                    this.props.favorite ? (<img src={iconFavorite} alt="" className={this.props.imgClass}/>) : <img src={iconFavoriteEmpty} alt=""  className={this.props.imgClass}/>
                }
            </button>
        )
    }

}

function mapStateToProps(state){
    return state;
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ButtonFavorite);

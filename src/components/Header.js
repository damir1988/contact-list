import React from "react";
import { Link } from "react-router-dom";
import logo from "../assets/images/logo.svg";

const Header = (props) => {

    return(
        <header className="header">
            <Link to={'/'} className="logo">
                <img src={logo} alt="Typeqast" className="logo__img"/>
            </Link>
        </header>
    )

}

export default Header;

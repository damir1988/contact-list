import React from "react";
import { withRouter } from "react-router-dom";
import {bindActionCreators} from "redux";
import {ActionCreators} from "../actions";
import {connect} from "react-redux";

class FormControl extends React.Component{

    render() {

        return(
            <input autoFocus={this.props.autofocus} type={this.props.type} name={this.props.name} id={this.props.id} required={this.props.required} className={this.props.className} value={this.props.value} onChange={(event) => { this.props.valueChanged(event) }} placeholder={this.props.placeholder} />
        )
    }

}

function mapStateToProps(state){
    return state;
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(FormControl));

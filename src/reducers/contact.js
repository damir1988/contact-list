import createReducer from '../libs/createReducer'
import * as types from '../actions/types';
import {getContacts} from "../actions/contact";

export const contacts = createReducer(null, {
    [types.GET_CONTACTS](state, action){
        return JSON.parse(localStorage.getItem("@contactlist"));
    }
});

export const contact = createReducer(null, {
    [types.GET_CONTACT](state, action){

        var contacts = JSON.parse(localStorage.getItem("@contactlist"));
        var id = action.payload;

        return contacts.find(contact => contact.id === id);
    }
});

export const deleteId = createReducer(null, {
    [types.SET_DELETE_ID](state, action){
        return action.payload;
    }
});

export const saved = createReducer(false, {
    [types.SET_SAVED](state, action){
        return action.payload;
    }
});


import { combineReducers } from 'redux';
import * as contactReducer from './contact';

export default combineReducers(Object.assign(contactReducer));

/* Selector - sort contacts */
export const sortContacts = (contacts) => {

    return contacts ? contacts.sort((a, b) => b.id - a.id) : contacts;

}

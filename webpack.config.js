const path = require('path');
const ip = require('ip');
const HWP = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: path.join(__dirname, '/src/index.js'),
    output: {
        filename: 'bundle.js',
        path: path.join(__dirname, '/dist'),
        publicPath: "/"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.sass$/,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'url-loader?limit=10000'
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin(
            {filename: '[name].css'}
        ),
        new HWP(
            {template: path.join(__dirname, '/src/index.html')}
        )
    ],
    devServer: {
        historyApiFallback: true,
        host: ip.address(),
        port: 8081,
    },
}
